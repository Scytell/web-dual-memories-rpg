# Site de création de personnages Dual Memory

## Présentation du site
Dans ce site, vous pourrez créer, modifier, ainsi qu'exporter vos personnages en PDF.
Veuillez noter qu'une consultation de votre Maître du jeu est fortement conseillée avant toute impression.

N'oubliez pas de lancer la commande suivante afin de pouvoir accéder aux données.
```
$ json-server --watch chara.json
```

***
Copyright © 2018, Ludovic OLIVET, Donovan ANTOINE